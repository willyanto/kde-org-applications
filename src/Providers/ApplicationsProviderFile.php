<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Providers;

use App\Model\Category;

/**
 * Class ApplicationsProviderFile
 * @package App\Providers
 *
 * Load applications list from a file
 */
class ApplicationsProviderFile
{
    private $raw;

    /**
     * ApplicationsProviderFile constructor.
     * @param string $filename
     */
    public function __construct(string $filename)
    {
        $this->raw = json_decode(file_get_contents($filename), true);
    }

    /**
     * @return \Generator
     */
    public function getAllCategories(): \Generator
    {
        $categories = array_keys($this->raw);
        sort($categories);
        foreach ($categories as $categoryName) {
            yield new Category($categoryName, $this->raw[$categoryName]);
        }
    }

    /**
     * @param string $category
     * @return Category
     */
    public function getApplicationsByCategory(string $category): ?Category
    {
        if ($this->raw[$category]) {
            return new Category($category, $this->raw[$category]);
        }
        return null;
    }
}
