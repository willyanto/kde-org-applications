<?php
/**
 * SPDX-FileCopyrightText: 2020 David Barchiesi <david@barchie.si>
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Model;

use App\Model\ReleaseType;

class Release
{
    private $version = null;
    /** @var ReleaseType */
    private $type;
    private $timestamp = null;
    private $artifacts = [];
    private $url;
    private $description;

    public function __construct(string $version, int $type, string $timestamp, ?array $description, ?string $url, array $artifacts)
    {
        $this->version = $version;
        $this->type = $type;
        $this->timestamp = $timestamp;
        $this->description = $description;
        $this->url = $url;
        $this->artifacts = $artifacts;
    }

    public static function fromData(array $release): ?Release
    {
        $url = isset($release['url']) && isset($release['url']['details']) ? $release['url']['details'] : null;
        $artifacts = isset($release['artifacts']) ? $release['artifacts'] : [];
        $description = isset($release['description']) ? $release['description'] : null;
        if ($release['type'] === 'stable') {
            return new Release($release['version'], ReleaseType::Stable,  $release['unix-timestamp'], $description, $url, $artifacts);
        } else if ($release['type'] === 'development') {
            return new Release($release['version'], ReleaseType::Development, $release['unix-timestamp'], $description, $url, $artifacts);
        } else  {
            return null;
        }
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * @return int|null
     */
    public function getTimestamp(): ?int
    {
        return $this->timestamp;
    }

    /**
     * @return array
     */
    public function getArtifacts(): array
    {
        return $this->artifacts;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return array|null
     */
    public function getDescription(): ?array
    {
        return $this->description;
    }
}
