<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Service;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;


class CdnCache
{
    /** @var array|null $cssFiles */
    private $cssFiles;

    /** @var array|null $jsFiles */
    private $jsFiles;

    public function __construct()
    {
        $cache = new FilesystemAdapter();
        $cache->delete('cdnFiles');

        $cdn = 'https://cdn.kde.org';
        $cdnPathPrefix = 'aether-devel';

        // carl dev config
        //$cdn = 'https://kde.carlschwan.eu';
        //$cdnPathPrefix = 'build';

        $cdnManifest = $cdn . '/' . $cdnPathPrefix . '/version/manifest.json';

        $cdnCSSFiles = ['/version/bootstrap.css', '/version/kde-org/applications.css'];
        $cdnJSFiles = ['/version/bootstrap.js', '/version/kde-org/applications.js'];

        ini_set('realpath_cache_size', 0);

        $cdnFiles = $cache->get('cdnFiles', function (ItemInterface $item) use ($cdnManifest, $cdnPathPrefix, $cdnCSSFiles, $cdnJSFiles) {
            $item->expiresAfter(600);
            $fileContent = file_get_contents($cdnManifest . "?rrere");
            $manifestData = json_decode($fileContent, true);

            $convertPaths = function($cdnCSSFile) use ($cdnPathPrefix, $manifestData)  {
                return $manifestData[$cdnPathPrefix . $cdnCSSFile];
            };
            return [
                'css' => array_map($convertPaths, $cdnCSSFiles),
                'js' => array_map($convertPaths, $cdnJSFiles),
            ];
        });

        $this->cssFiles = $cdnFiles['css'];
        $this->jsFiles = $cdnFiles['js'];
    }

    public function getCssFiles(): array
    {
        return $this->cssFiles;
    }

    public function getJsFiles(): array
    {
        return $this->jsFiles;
    }
}
