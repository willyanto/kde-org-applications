#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2020-07-27 17:41-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.3\n"

msgid "KDE's Applications"
msgstr "Aplicativos do KDE"

msgid ""
"KDE is a community of friendly people who create over 200 apps which run on "
"any Linux desktop, and often other platforms too. Here is the complete list."
msgstr ""
"O KDE é uma comunidade de pessoas amigáveis que criam mais de 200 "
"aplicativos que são executados em qualquer computador com Linux e também em "
"outras plataformas. Aqui está a lista completa."

msgid "Language selector"
msgstr "Seletor de idioma"

msgid "Selected language: English"
msgstr "Idioma selecionado: inglês"

msgid "Selected language: %lang%"
msgstr "Idioma selecionado: %lang%"

msgid "Search applications"
msgstr "Pesquisar aplicativos"

msgid "Search..."
msgstr "Pesquisar..."

msgid "%name% Icon"
msgstr "Ícone do %name%"

msgid "Get Involved"
msgstr "Contribua"

msgid "Source Code Repository"
msgstr "Repositório do código-fonte"

msgid "Browse %name% source code online"
msgstr "Navegue online pelo código-fonte do %name%"

msgid "Install on Linux"
msgstr "Instale no Linux"

msgid "This app is unmaintained and no longer released by the KDE community."
msgstr "Este aplicativo não é mais mantido e nem lançado pela comunidade KDE."

msgid "Screenshots"
msgstr "Capturas de tela"

msgid "Screeenshot of %name%"
msgstr "Captura de tela do %name%"

msgid ""
"This button only works with <a\n"
"href=\"/applications/system/org.kde.discover\">Discover</a> and other\n"
"AppStream application stores. You can also use your distribution's package "
"manager."
msgstr ""
"Este botão só funciona com o <a\n"
"href=\"/applications/system/org.kde.discover\">Discover</a> e outras\n"
"lojas de aplicativos AppStream. Você também pode usar o gerenciador de "
"pacotes de sua distribuição."

msgid "Get it from Microsoft"
msgstr "Baixe da Microsoft"

msgid "Install on Windows"
msgstr "Instale no Windows"

msgid "Get it from F-Droid"
msgstr "Baixe do F-Droid"

msgid "Get it on F-Droid"
msgstr "Baixe no F-Droid"

msgid "Get it on Google Play"
msgstr "Baixe do Google Play"

msgid "Releases"
msgstr "Versões"

msgid "Learn more"
msgstr "Saiba mais"

msgid "AppImage"
msgstr "AppImage"

msgid "macOS"
msgstr "macOS"

msgid "Windows"
msgstr "Windows"

msgid "Show more releases..."
msgstr "Mostrar mais versões..."

msgid "Details for %name%"
msgstr "Detalhes do %name%"

msgid "Project website"
msgstr "Site do projeto"

msgid "Latest stable release"
msgstr "Última versão estável"

msgid "Latest development release"
msgstr "Última versão de desenvolvimento"

msgid "License"
msgstr "Licença"

msgid "Get Help"
msgstr "Obter ajuda"

msgid "%name% Handbook"
msgstr "Manual do %name%"

msgid "KDE Community Forum"
msgstr "Fórum da comunidade KDE (em inglês)"

msgid "Contact the authors"
msgstr "Contate os autores"

msgid "Report a bug"
msgstr "Relate um erro"

msgid "IRC:"
msgstr "IRC:"

msgid "Mailing List:"
msgstr "Lista de discussão:"

msgid "Author(s):"
msgstr "Autores:"

msgid "Thanks to:"
msgstr "Agradecimentos a:"

msgid "Development information"
msgstr "Informação de desenvolvimento"

msgid "Development"
msgstr "Desenvolvimento"

msgid "Education"
msgstr "Educação"

msgid "Games"
msgstr "Jogos"

msgid "Graphics"
msgstr "Gráficos"

msgid "Internet"
msgstr "Internet"

msgid "Multimedia"
msgstr "Multimídia"

msgid "Office"
msgstr "Escritório"

msgid "System"
msgstr "Sistema"

msgid "Utilities"
msgstr "Utilitários"

msgid "Products"
msgstr "Produtos"

msgid "Develop"
msgstr "Desenvolvimento"

msgid "Donate"
msgstr "Doar"

msgid "Unmaintained"
msgstr "Não mantidos"

msgid "Applications"
msgstr "Aplicativos"

msgid "Addons"
msgstr "Extensões"
